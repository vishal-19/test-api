package com.demo.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Main runner class for test, provide the features directory or tags and html report generation path
 *
 * @author vishal
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = {"classpath:features"}, tags = {"@check"}, plugin = {"html:target/report.html"})
public class TestsRunner {

}
