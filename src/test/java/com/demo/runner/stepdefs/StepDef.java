package com.demo.runner.stepdefs;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

import java.io.File;

/**
 * This class is step definition, here all the feature file steps are defined
 *
 * @author vishal
 */
public class StepDef {

    private Scenario scenario;
    private String endPoint;
    private Response apiResponse;
    private String requestBody;

    /**
     * This method is called before each scenario, we can use scenario in other methods below to write details into html report
     *
     * @param scenario
     */
    @Before
    public void beforeScenario(Scenario scenario) {
        this.scenario = scenario;
    }


    @Given("^we have the service endpoint (.+)$")
    public void we_have_the_service_endpoint(String endpoint) {
        this.endPoint = endpoint;
    }

    @When("^get request is sent$")
    public void get_request_is_sent() {
        apiResponse = RestAssured.given().when().get(this.endPoint);
    }


    @And("^request body is set as (.+)$")
    public void request_body_is_set(String fileName) throws Throwable {
        requestBody = FileUtils.readFileToString(new File("src/test/resources/requests/" + fileName), "UTF-8");
    }

    @When("^post request is sent$")
    public void post_request_is_sent() {
        apiResponse = RestAssured.given().when().body(this.requestBody).post(this.endPoint);
    }

    @Then("^status code should be (.+)$")
    public void status_code_should_be(int statusCode) {
        scenario.write("status code from service is " + apiResponse.statusCode());
        Assert.assertEquals(statusCode, apiResponse.getStatusCode());
    }

    @Then("^valid get response should come$")
    public void valid_get_response_should_come() throws Throwable {
        File file = new File("src/test/resources/response/getResponse.json");
        String expectedResponse = FileUtils.readFileToString(file, "UTF-8");
        scenario.write(apiResponse.getBody().asString());
        JSONAssert.assertEquals(expectedResponse, apiResponse.getBody().asString(), JSONCompareMode.STRICT);
    }

    @Then("^valid post response should come$")
    public void valid_post_response_should_come() throws Throwable {
        File file = new File("src/test/resources/response/postResponse.json");
        String expectedResponse = FileUtils.readFileToString(file, "UTF-8");
        scenario.write(apiResponse.getBody().asString());
        JSONAssert.assertEquals(expectedResponse, apiResponse.getBody().asString(), new CustomComparator(
                JSONCompareMode.STRICT,
                Customization.customization("data.id", // except this field all needs to be validated
                        (o1, o2) -> {
                            return true;
                        })));
    }

}
