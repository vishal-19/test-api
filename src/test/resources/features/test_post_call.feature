@check
Feature: Test post call of api

  Scenario: Check post call of api is giving 200 response status code

    Given we have the service endpoint http://dummy.restapiexample.com/api/v1/create
    And request body is set as tempRequest.json
    When post request is sent
    Then status code should be 200


  Scenario: Check the post api is responding properly
    Given we have the service endpoint http://dummy.restapiexample.com/api/v1/create
    And request body is set as tempRequest.json
    When post request is sent
    Then valid post response should come


