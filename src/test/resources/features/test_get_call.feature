@check
Feature: Test get call of api

  Scenario: Check get call of api is giving 200 response status code

    Given we have the service endpoint http://dummy.restapiexample.com/api/v1/employees
    When get request is sent
    Then status code should be 200

  Scenario: Check the get api is responding properly
    Given we have the service endpoint http://dummy.restapiexample.com/api/v1/employees
    When get request is sent
    Then valid get response should come


